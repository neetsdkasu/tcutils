package tcapi

// author: Leonardone

// TopCoder API
// tcapi.docs.apiary.io/

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	"net/url"
	"path/filepath"
)

const (
	PublicProfileApi       = "v2/users/%s"
	PublicProfileDir       = "users"
	PubicProfileExpireDays = 7
)

func MakePublicProfileApiFileName(handle string) string {
	return filepath.Join(PublicProfileDir, hex.EncodeToString([]byte(handle)))
}

func MakePublicProfileApiPath(handle string) string {
	return fmt.Sprintf(PublicProfileApi, url.PathEscape(handle))
}

type PublicProfile map[string]interface{}

func (p PublicProfile) Country() string {
	if p == nil {
		return ""
	}
	if value, ok := p["country"]; ok {
		switch country := value.(type) {
		case string:
			return country
		default:
			return ""
		}
	} else {
		return ""
	}
}

func GetPublicProfile(handle string, updating data.Updating) (PublicProfile, error) {
	path := MakePublicProfileApiPath(handle)
	fileName := MakePublicProfileApiFileName(handle)
	d := NewData(path, fileName, PubicProfileExpireDays, updating)
	var v interface{}
	if err := d.Scan(&v); err != nil {
		return nil, err
	}
	return PublicProfile(v.(map[string]interface{})), nil
}
