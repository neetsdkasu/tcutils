package tcapi

// author: Leonardone

// TopCoder API
// tcapi.docs.apiary.io/

import (
	"gitlab.com/neetsdkasu/tcutils/data"
	"path/filepath"
)

const (
	ApiServer = "api.topcoder.com"
	ApiDir    = "api"
)

func NewData(path, fileName string, expireDays int, updateing data.Updating) *data.Data {
	ret := new(data.Data)
	ret.Secure = true
	ret.Server = ApiServer
	ret.Path = path
	ret.FilePath = filepath.Join(ApiDir, fileName+".json")
	ret.ExpireDays = expireDays
	ret.Updating = updateing
	ret.DataType = data.Json
	return ret
}
