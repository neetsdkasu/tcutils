package main

// author: Leonardone

import (
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	"gitlab.com/neetsdkasu/tcutils/tcapi"
	. "gitlab.com/neetsdkasu/tcutils/tcmmdatafeed"
	"log"
	"math"
)

type Detail struct {
	tcapi.PublicProfile
	*IndividualResults
	ProvisionalPlaced, AverageTime, Bests, Uniques, Zeros int
}

func Details(rr *RoundResults, updatingPublicProfile, updatingIndividualResults data.Updating) ([]*Detail, error) {
	details := make([]*Detail, len(rr.Results))
	pstandings := rr.ProvisionalStandings()
	for i, r := range rr.Results {
		log.Println(i+1, "/", len(details))
		pf, err := tcapi.GetPublicProfile(r.Handle, updatingPublicProfile)
		if err != nil {
			return nil, err
		}
		ir, err := GetIndividualResults(r.RoundId, r.CoderId, updatingIndividualResults)
		if err != nil {
			return nil, err
		}
		at := ir.AverageTime()
		zr := ir.Zeros()
		details[i] = &Detail{pf, ir, pstandings[i], at, 0, 0, zr}
	}
	var update func(a, b float64) bool
	testCaseCount := len(details[0].TestCases)
	e := len(details) / 2
	less := 0
	for i := 0; i < testCaseCount; i++ {
		if details[0].TestCases[i].Score < details[e].TestCases[i].Score {
			less++
		}
	}
	bestScores := make([]float64, testCaseCount)
	bestCounts := make([]int, testCaseCount)
	if less > testCaseCount-less {
		update = func(a, b float64) bool { return a > 0.0 && a < b }
		for i := range bestScores {
			bestScores[i] = math.MaxFloat64
		}
	} else {
		update = func(a, b float64) bool { return a > b }
	}
	for _, dt := range details {
		for i, tc := range dt.TestCases {
			if update(tc.Score, bestScores[i]) {
				bestScores[i] = tc.Score
				bestCounts[i] = 1
			} else if tc.Score == bestScores[i] {
				bestCounts[i]++
			}
		}
	}
	for j, dt := range details {
		s := 0.0
		for i, tc := range dt.TestCases {
			if tc.Score == bestScores[i] {
				dt.Bests++
				if bestCounts[i] == 1 {
					dt.Uniques++
				}
			}
			if bestScores[i] > 0.0 {
				s += tc.Score / bestScores[i]
			}
		}
		log.Println(fmt.Sprintf("%s's score %f", rr.Results[j].Handle, s))
	}
	return details, nil
}
