package main

// author: Leonardone

import (
	"flag"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	. "gitlab.com/neetsdkasu/tcutils/selmmround"
	. "gitlab.com/neetsdkasu/tcutils/tcmmdatafeed"
	"log"
	"os"
)

var autoFlag bool
var forceFlag bool
var offlineFlag bool

func init() {
	flag.BoolVar(&autoFlag, "auto", false, "auto updating")
	flag.BoolVar(&forceFlag, "force", false, "must update")
	flag.BoolVar(&offlineFlag, "offline", false, "offline (no update, no download)")
}

func main() {

	flag.Parse()

	updatingRoundList := data.AutoUpdate
	updatingRoundResults := data.NoUpdate
	updatingPublicProfile := data.NoUpdate
	updatingIndividualResults := data.NoUpdate

	if autoFlag {
		updatingRoundResults = data.AutoUpdate
		updatingPublicProfile = data.AutoUpdate
		updatingIndividualResults = data.AutoUpdate
	}

	if forceFlag {
		updatingRoundList = data.ForceUpdate
		updatingRoundResults = data.ForceUpdate
		updatingPublicProfile = data.ForceUpdate
		updatingIndividualResults = data.ForceUpdate
	}

	if offlineFlag {
		updatingRoundList = data.Offline
		updatingRoundResults = data.Offline
		updatingPublicProfile = data.Offline
		updatingIndividualResults = data.Offline
	}

	round, err := SelectRound(updatingRoundList)
	if err != nil {
		if err == Canceled {
			log.Println(err)
			return
		}
		log.Panic(err)
	}
	filename := "statistics.txt"
	file, err := os.Create(filename)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()
	fmt.Println("Contest: ", round.FullName)
	fmt.Println("RoundId: ", round.RoundId)
	fmt.Println("Date: ", round.Date)
	fmt.Fprintln(file, "Contest: ", round.FullName)
	fmt.Fprintln(file, "RoundId: ", round.RoundId)
	fmt.Fprintln(file, "Date: ", round.Date)
	fmt.Fprintln(file, "Statistics")
	fmt.Fprintln(file, `<pre><font size="3">`)
	results, err := GetRoundResults(round.RoundId, updatingRoundResults)
	if err != nil {
		log.Panic(err)
	}
	details, err := Details(results, updatingPublicProfile, updatingIndividualResults)
	if err != nil {
		log.Panic(err)
	}
	fmt.Fprintln(file, fmt.Sprintf(
		"<b>%-3s %-24s %-15s %-9s %-3s %-9s %-4s %-4s %-4s %-4s &#9650;&#9660;</b>",
		"POS", "HANDLE", "COUNTRY", "SCORE", "PRP", "PRSCORE",
		"BEST", "UNIQ", "ZERO", "TIME",
	))
	changes := func(a, b int) string {
		if a < b {
			return fmt.Sprintf(`<font color="green">&#9650;%d</font>`, b-a)
		} else if a > b {
			return fmt.Sprintf(`<font color="red">&#9660;%d</font>`, a-b)
		} else {
			return "&#9642;0"
		}
	}
	for i, r := range results.Results {
		dt := details[i]
		pf := dt.PublicProfile
		fmt.Fprintln(file, fmt.Sprintf(
			"%3d %-27s %-15s %9.2f %3d %9.2f %4d %4d %4d %4d %s",
			r.Placed, "[h]"+r.Handle+"[/h]", pf.Country(), r.FinalScore,
			dt.ProvisionalPlaced, r.ProvisionalScore,
			dt.Bests, dt.Uniques, dt.Zeros, dt.AverageTime,
			changes(r.Placed, dt.ProvisionalPlaced),
		))
	}
	fmt.Fprintln(file, "</font></pre>")
	log.Println("saved", filename)
}
