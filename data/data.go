package data

import (
	"bufio"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"
)

type DataType int

const (
	Xml DataType = iota
	Json
)

type Updating int

const (
	Offline     Updating = iota // not download
	NoUpdate                    // not download if it exists
	AutoUpdate                  // not download before expire day
	ForceUpdate                 // download
)

type Data struct {
	Secure     bool
	Server     string
	Path       string
	FilePath   string
	ExpireDays int
	DataType
	Updating
}

var (
	lastAccess     = make(map[string]time.Time)
	AccessInterval = 5 * time.Second
)

func (data *Data) Scan(v interface{}) error {
	if data == nil {
		return fmt.Errorf("data is nil")
	}
	if err := data.update(); err != nil {
		return err
	}
	switch data.DataType {
	case Xml:
		return data.scanXml(v)
	case Json:
		return data.scanJson(v)
	default:
		return fmt.Errorf("unknown data.DataType (%v)", data.DataType)
	}
}

func (data *Data) update() error {

	// check updating
	switch data.Updating {
	case Offline:
		return nil
	case NoUpdate:
		if _, err := os.Stat(data.FilePath); os.IsExist(err) {
			return err
		} else if !os.IsNotExist(err) {
			return nil
		}
	case AutoUpdate:
		if fi, err := os.Stat(data.FilePath); os.IsExist(err) {
			return err
		} else if !os.IsNotExist(err) {
			if time.Now().Before(fi.ModTime().AddDate(0, 0, data.ExpireDays)) {
				return nil
			}
		}
	case ForceUpdate:
		break
	default:
		return fmt.Errorf("unknown data.Updating (%v)", data.Updating)
	}

	log.Println("update:", data.Server, data.Path, data.FilePath)

	// control access interval
	if la, ok := lastAccess[data.Server]; ok {
		ex := la.Add(AccessInterval)
		time.Sleep(time.Until(ex))
	}
	defer func() {
		lastAccess[data.Server] = time.Now()
	}()

	// download
	var url string
	if data.Secure {
		url = "https"
	} else {
		url = "http"
	}
	url += "://" + path.Join(data.Server, data.Path)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// save to file
	if err := os.MkdirAll(filepath.Dir(data.FilePath), os.ModeDir); err != nil {
		return err
	}
	file, err := os.Create(data.FilePath)
	if err != nil {
		return nil
	}
	defer file.Close()
	reader := bufio.NewReader(resp.Body)
	_, err = reader.WriteTo(file)
	return err
}

func CharsetReader(charset string, input io.Reader) (io.Reader, error) {
	// change charset to UTF-8
	// no implements (almost, datafeeds have only ascii chars)
	return input, nil
}

func (data *Data) scanXml(v interface{}) error {
	file, err := os.Open(data.FilePath)
	if err != nil {
		return err
	}
	defer file.Close()
	decoder := xml.NewDecoder(file)
	decoder.CharsetReader = CharsetReader
	return decoder.Decode(v)
}

func (data *Data) scanJson(v interface{}) error {
	buf, err := ioutil.ReadFile(data.FilePath)
	if err != nil {
		return err
	}
	return json.Unmarshal(buf, v)
}
