package selmmround

// author: Leonardone

import (
	"bufio"
	"errors"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	. "gitlab.com/neetsdkasu/tcutils/tcmmdatafeed"
	"log"
	"os"
	"strconv"
	"strings"
)

var Canceled = errors.New("Canceled Select Round")

func SelectRound(updating data.Updating) (*Round, error) {
	roundList, err := GetRoundList(updating)
	if err != nil {
		return nil, err
	}
	list := []*Round{}
	list = append(list, roundList.Rounds[:15]...)
	index := -1
	scanner := bufio.NewScanner(os.Stdin)
	showFullName := false
	for index < 0 {
		for i, r := range list {
			if showFullName {
				fmt.Println(fmt.Sprintf("%6d: %s", i+1, r.FullName))
			} else {
				fmt.Println(fmt.Sprintf("%6d: %s", i+1, r.ShortName))
			}
		}
		fmt.Println(len(list), "/", len(roundList.Rounds))
		fmt.Println("command list [r:<regex>, s:<findstr>, j:<offset>, exit, full, short]")
		fmt.Print("index or command: ")
		scanner.Scan()
		input := scanner.Text()
		if strings.HasPrefix(input, "r:") {
			expr := strings.TrimPrefix(input, "r:")
			err = FilterByRegexp(&list, roundList.Rounds, expr)
			if err != nil {
				log.Println(err.Error())
			}
		} else if strings.HasPrefix(input, "s:") {
			substr := strings.TrimPrefix(input, "s:")
			FilterBySubstr(&list, roundList.Rounds, substr)
		} else if strings.HasPrefix(input, "j:") {
			substr := strings.TrimPrefix(input, "j:")
			temp, err := strconv.Atoi(substr)
			if err != nil {
				log.Println(err.Error())
			} else if temp < 1 || len(roundList.Rounds) < temp {
				log.Println("invalid offset: ", temp)
			} else {
				from := temp - 1
				to := from + 15
				if to > len(roundList.Rounds) {
					to = len(roundList.Rounds)
				}
				list = list[:0]
				list = append(list, roundList.Rounds[from:to]...)
			}
		} else if input == "exit" {
			return nil, Canceled
		} else if input == "full" {
			showFullName = true
		} else if input == "short" {
			showFullName = false
		} else {
			temp, err := strconv.Atoi(input)
			if err != nil {
				log.Println(err.Error())
			} else if temp < 1 || len(list) < temp {
				log.Println("invalid index: ", temp)
			} else {
				index = temp
			}
		}
	}
	return list[index-1], nil
}
