package selmmround

// author: Leonardone

import (
	. "gitlab.com/neetsdkasu/tcutils/tcmmdatafeed"
	"regexp"
	"strings"
)

func FilterByRegexp(dst *[]*Round, src []*Round, expr string) error {
	list := (*dst)[:0]
	if len(expr) == 0 {
		return nil
	}
	re, err := regexp.Compile(expr)
	if err != nil {
		return err
	}
	for _, r := range src {
		if re.MatchString(r.ShortName) || re.MatchString(r.FullName) {
			list = append(list, r)
		}
	}
	(*dst) = list
	return nil
}

func FilterBySubstr(dst *[]*Round, src []*Round, substr string) {
	list := (*dst)[:0]
	if len(substr) == 0 {
		return
	}
	for _, r := range src {
		if strings.Contains(r.ShortName, substr) || strings.Contains(r.FullName, substr) {
			list = append(list, r)
		}
	}
	(*dst) = list
}
