package main

// author: Leonardone

// Marathon Match Rating Calculation
// community.topcoder.com/longcontest/?module=Static&d1=support&d2=ratings

import (
	"flag"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	. "gitlab.com/neetsdkasu/tcutils/selmmround"
	. "gitlab.com/neetsdkasu/tcutils/tcmmdatafeed"
	"log"
	"math"
)

func calcRatings(coders []*Result) {

	if len(coders) < 2 {
		return
	}

	numCoders := float64(len(coders))

	aveRating := 0.0
	for _, coder := range coders {
		aveRating += float64(coder.OldRating)
	}
	aveRating /= numCoders

	cf := 0.0
	{
		cf1 := 0.0
		cf2 := 0.0
		for _, coder := range coders {
			cf1 += math.Pow(float64(coder.OldVolatility), 2.0)
			cf2 += math.Pow(float64(coder.OldRating)-aveRating, 2.0)
		}
		cf1 /= numCoders
		cf2 /= numCoders - 1.0
		cf = math.Sqrt(cf1 + cf2)
	}

	eRanks := make([]float64, len(coders))
	ePerfs := make([]float64, len(coders))
	for i, coder1 := range coders {
		wp := 0.5
		for _, coder2 := range coders {
			tmp1 := float64(coder2.OldRating - coder1.OldRating)
			v1 := math.Pow(float64(coder1.OldVolatility), 2.0)
			v2 := math.Pow(float64(coder2.OldVolatility), 2.0)
			tmp2 := math.Sqrt(2.0 * (v1 + v2))
			wp += 0.5 * (math.Erf(tmp1/tmp2) + 1.0)
		}
		eRanks[i] = wp
		ePerfs[i] = -stdNorminv((eRanks[i] - 0.5) / numCoders)
	}

	aRanks := make([]float64, len(coders))
	aPerfs := make([]float64, len(coders))
	{
		rankSums := make([]int, coders[len(coders)-1].Placed+1)
		rankCounts := make([]int, len(rankSums))
		for i, coder := range coders {
			rankSums[coder.Placed] += i + 1
			rankCounts[coder.Placed]++
		}
		for i, coder := range coders {
			sum := float64(rankSums[coder.Placed])
			count := float64(rankCounts[coder.Placed])
			aRanks[i] = sum / count
			aPerfs[i] = -stdNorminv((aRanks[i] - 0.5) / numCoders)
		}
	}

	newRatings := make([]float64, len(coders))
	newVolatilities := make([]float64, len(coders))

	calc := func(i int, coder *Result) {
		perfAs := float64(coder.OldRating) + cf*(aPerfs[i]-ePerfs[i])

		weight := 1.0/(1.0-(0.42/float64(coder.NumRatings)+0.18)) - 1.0
		if coder.OldRating > 2500 {
			weight *= 0.8
		} else if coder.OldRating >= 2000 {
			weight *= 0.9
		}

		Cap := 150.0 + 1500.0/float64(coder.NumRatings+1)

		newRatings[i] = math.Max(
			float64(coder.OldRating)-Cap,
			math.Min(
				float64(coder.OldRating)+Cap,
				(float64(coder.OldRating)+weight*perfAs)/(1.0+weight),
			))
		if coder.Placed == 1 && newRatings[i] <= float64(coder.OldRating) {
			newRatings[i] = float64(coder.OldRating + 1)
		}

		newVolatilities[i] = math.Sqrt(
			math.Pow(newRatings[i]-float64(coder.OldRating), 2.0)/weight +
				math.Pow(float64(coder.OldVolatility), 2.0)/(weight+1.0))
	}

	for i, coder := range coders {
		calc(i, coder)
	}

	fmt.Println(fmt.Sprintf(
		"%-3s %-20s %-3s %-4s %-4s %-4s %-4s %-5s %-5s %-7s %-6s",
		"Pos", "Handle", "Num",
		"OldR", "OldV",
		"NewR", "NewV",
		"ARank", "ERank",
		"Rating", "Vol",
	))

	toStr := func(i int, coder *Result) string {
		return fmt.Sprintf(
			"%3d %-20s %3d %4d %4d %4d %4d %5.1f %5.1f %7.2f %6.2f",
			coder.Placed, coder.Handle, coder.NumRatings,
			coder.OldRating, coder.OldVolatility,
			coder.NewRating, coder.NewVolatility,
			aRanks[i], eRanks[i],
			newRatings[i], newVolatilities[i],
		)
	}

	isValid := func(i int, coder *Result) bool {
		return coder.NewRating == int(math.Round(newRatings[i])) &&
			coder.NewVolatility == int(math.Round(newVolatilities[i]))
	}

	correct := 0
	incorrects := []int{}
	for i, coder := range coders {
		fmt.Println(toStr(i, coder))
		if isValid(i, coder) {
			correct++
		} else {
			incorrects = append(incorrects, i)
		}
	}
	fmt.Println("---------------------------------------")
	fmt.Println("correct:", correct)
	fmt.Println("incorrect:", len(incorrects))
	for _, i := range incorrects {
		fmt.Println(toStr(i, coders[i]))
	}

	if len(incorrects) == 0 {
		return
	}

	fixed := []int{}
	unfixed := []int{}
	for _, i := range incorrects {
		coder := coders[i]
		num := coder.NumRatings
		ok := false
		for j := 1; j < 50; j++ {
			coder.NumRatings = num + j
			calc(i, coder)
			if isValid(i, coder) {
				ok = true
				break
			}
			if num-j > 0 {
				coder.NumRatings = num - j
				calc(i, coder)
				if isValid(i, coder) {
					ok = true
					break
				}
			}
		}
		if ok {
			fixed = append(fixed, i)
		} else {
			unfixed = append(unfixed, i)
			coder.NumRatings = num
			calc(i, coder)
		}
	}

	fmt.Println("---------------------------------------")
	fmt.Println("fixed:", len(fixed))
	for _, i := range fixed {
		fmt.Println(toStr(i, coders[i]))
	}

	fmt.Println("---------------------------------------")
	fmt.Println("unfixed:", len(unfixed))
	for _, i := range unfixed {
		fmt.Println(toStr(i, coders[i]))
	}
}

func stdNorminv(p float64) float64 {
	return math.Sqrt2 * math.Erfinv(2.0*p-1.0)
}

var autoFlag bool
var forceFlag bool
var offlineFlag bool

func init() {
	flag.BoolVar(&autoFlag, "auto", false, "auto updating")
	flag.BoolVar(&forceFlag, "force", false, "must update")
	flag.BoolVar(&offlineFlag, "offline", false, "offline (no update, no download)")
}

func main() {

	flag.Parse()

	updatingRoundList := data.AutoUpdate
	updatingRoundResult := data.NoUpdate
	updatingRatingHistory := data.NoUpdate

	if autoFlag {
		updatingRoundResult = data.AutoUpdate
		updatingRatingHistory = data.AutoUpdate
	}

	if forceFlag {
		updatingRoundList = data.ForceUpdate
		updatingRoundResult = data.ForceUpdate
		updatingRatingHistory = data.ForceUpdate
	}

	if offlineFlag {
		updatingRoundList = data.Offline
		updatingRoundResult = data.Offline
		updatingRatingHistory = data.Offline
	}

	round, err := SelectRound(updatingRoundList)
	if err != nil {
		if err == Canceled {
			log.Println(err)
			return
		}
		log.Panic(err)
	}

	roundResults, err := GetRoundResults(round.RoundId, updatingRoundResult)
	if err != nil {
		log.Panic(err)
	}

	coders := []*Result{}
	for _, res := range roundResults.Results {
		if res.OldRating > 0 {

			hist, err := GetRatingHistory(res.CoderId, updatingRatingHistory)
			if err != nil {
				log.Panic(err)
			}

			if num := hist.GetNumRatingsOnRound(round.RoundId); num == 0 {
				log.Println(fmt.Errorf(
					"%s's num_ratings is 0 (cr=%d, rd=%d, %s)",
					res.Handle,
					res.CoderId,
					round.RoundId,
					round.ShortName,
				))
			} else {
				res.NumRatings = num
			}

			coders = append(coders, res)
		}
	}

	calcRatings(coders)
}
