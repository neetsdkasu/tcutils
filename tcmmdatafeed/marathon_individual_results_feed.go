package tcmmdatafeed

// author: Leonardone

// TopCoder MM DataFeeds
// community.topcoder.com/longcontest/?module=Static&d1=support&d2=dataFeed

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	"path/filepath"
	"sort"
)

const (
	IndividualResultsPath       = "longcontest/stats/?module=IndividualResultsFeed&rd=%d&cr=%d"
	IndividualResultsDir        = "individual_results"
	IndividualResultsExpireDays = 6000
)

func MakeIndividualResultsFileName(rd, cr int64) string {
	return filepath.Join(IndividualResultsDir, fmt.Sprint(rd), fmt.Sprint(cr))
}

func MakeIndividualResultsPath(rd, cr int64) string {
	return fmt.Sprintf(IndividualResultsPath, rd, cr)
}

type Submission struct {
	Number   int     `xml:"number"`
	Score    float64 `xml:"score"`
	Language string  `xml:"language"`
	Time     string  `xml:"time"`
}

type TestCase struct {
	TestCaseId     int64   `xml:"test_case_id"`
	Score          float64 `xml:"score"`
	ProcessingTime int     `xml:"processing_time"`
	FatalErrorInd  int     `xml:"fatal_error_ind"`
}

type IndividualResults struct {
	XMLName     xml.Name      `xml:"marathon_individual_results"`
	RoundId     int64         `xml:"round_id"`
	CoderId     int64         `xml:"coder_id"`
	Handle      string        `xml:"handle"`
	Submissions []*Submission `xml:"submissions>submission"`
	TestCases   []*TestCase   `xml:"testcases>testcase"`
}

func GetIndividualResults(rd, cr int64, updating data.Updating) (*IndividualResults, error) {
	path := MakeIndividualResultsPath(rd, cr)
	fileName := MakeIndividualResultsFileName(rd, cr)
	d := NewData(path, fileName, IndividualResultsExpireDays, updating)
	v := new(IndividualResults)
	if err := d.Scan(v); err != nil {
		return nil, err
	}
	sort.Slice(v.Submissions, func(i, j int) bool {
		return v.Submissions[i].Number < v.Submissions[j].Number
	})
	sort.Slice(v.TestCases, func(i, j int) bool {
		return v.TestCases[i].TestCaseId < v.TestCases[j].TestCaseId
	})
	return v, nil
}

func (ir *IndividualResults) AverageTime() int {
	if ir == nil {
		return 0
	}
	sum := 0
	count := 0
	for _, tc := range ir.TestCases {
		if tc.ProcessingTime > 0 {
			sum += tc.ProcessingTime
			count++
		}
	}
	if count > 0 {
		return (sum + count - 1) / count
	} else {
		return 0
	}
}

func (ir *IndividualResults) Zeros() int {
	if ir == nil {
		return 0
	}
	count := 0
	for _, tc := range ir.TestCases {
		if tc.Score == 0.0 || tc.Score < 0.0 {
			count++
		}
	}
	return count
}
