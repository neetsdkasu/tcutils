package tcmmdatafeed

// author: Leonardone

// TopCoder MM DataFeeds
// community.topcoder.com/longcontest/?module=Static&d1=support&d2=dataFeed

import (
	"gitlab.com/neetsdkasu/tcutils/data"
	"path/filepath"
)

const (
	DataFeedServer = "www.topcoder.com"
	DataFeedDir    = "mmdatafeed"
)

func NewData(path, fileName string, expireDays int, updateing data.Updating) *data.Data {
	ret := new(data.Data)
	ret.Secure = true
	ret.Server = DataFeedServer
	ret.Path = path
	ret.FilePath = filepath.Join(DataFeedDir, fileName+".xml")
	ret.ExpireDays = expireDays
	ret.Updating = updateing
	ret.DataType = data.Xml
	return ret
}
