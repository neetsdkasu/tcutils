package tcmmdatafeed

// author: Leonardone

// TopCoder MM DataFeeds
// community.topcoder.com/longcontest/?module=Static&d1=support&d2=dataFeed

import (
	"encoding/xml"
	"gitlab.com/neetsdkasu/tcutils/data"
	"sort"
)

const (
	RoundListPath       = "tc?module=BasicData&c=dd_marathon_round_list"
	RoundListFileName   = "marathon_round_list"
	RoundListExpireDays = 7
)

type Round struct {
	RoundId   int64  `xml:"round_id"`
	FullName  string `xml:"full_name"`
	ShortName string `xml:"short_name"`
	Date      string `xml:"date"`
	RoundType string `xml:"round_type"`
}

type RoundList struct {
	XMLName xml.Name `xml:"dd_marathon_round_list"`
	Rounds  []*Round `xml:"row"`
}

func GetRoundList(updating data.Updating) (*RoundList, error) {
	d := NewData(RoundListPath, RoundListFileName, RoundListExpireDays, updating)
	v := new(RoundList)
	if err := d.Scan(v); err != nil {
		return nil, err
	}
	sort.Slice(v.Rounds, func(i, j int) bool {
		return v.Rounds[i].Date > v.Rounds[j].Date
	})
	return v, nil
}
