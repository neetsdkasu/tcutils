package tcmmdatafeed

// author: Leonardone

// TopCoder MM DataFeeds
// community.topcoder.com/longcontest/?module=Static&d1=support&d2=dataFeed

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	"path/filepath"
	"sort"
)

const (
	RoundResultsPath       = "tc?module=BasicData&c=dd_marathon_round_results&rd=%d"
	RoundResultsDir        = "marathon_round_results"
	RoundResultsExpireDays = 6000
)

func MakeRoundResultsFileName(rd int64) string {
	return filepath.Join(RoundResultsDir, fmt.Sprint(rd))
}

func MakeRoundResultsPath(rd int64) string {
	return fmt.Sprintf(RoundResultsPath, rd)
}

type Result struct {
	RoundId          int64   `xml:"round_id"`
	CoderId          int64   `xml:"coder_id"`
	Handle           string  `xml:"handle"`
	OldRating        int     `xml:"old_rating"`
	NewRating        int     `xml:"new_rating"`
	OldVolatility    int     `xml:"old_volatility"`
	NewVolatility    int     `xml:"new_volatility"`
	NumRatings       int     `xml:"num_ratings"`
	Placed           int     `xml:"placed"`
	Advanced         string  `xml:"advanced"`
	ProvisionalScore float64 `xml:"provisional_score"`
	FinalScore       float64 `xml:"final_score"`
	NumSubmissions   int     `xml:"num_submissions"`
	RatedFlag        int     `xml:"rated_flag"`
}

type RoundResults struct {
	XMLName xml.Name  `xml:"dd_marathon_round_results"`
	Results []*Result `xml:"row"`
}

func GetRoundResults(rd int64, updating data.Updating) (*RoundResults, error) {
	path := MakeRoundResultsPath(rd)
	fileName := MakeRoundResultsFileName(rd)
	d := NewData(path, fileName, RoundResultsExpireDays, updating)
	v := new(RoundResults)
	if err := d.Scan(v); err != nil {
		return nil, err
	}
	sort.Slice(v.Results, func(i, j int) bool {
		return v.Results[i].Placed < v.Results[j].Placed
	})
	return v, nil
}

func (rr *RoundResults) ProvisionalStandings() []int {
	standings := make([]int, len(rr.Results))
	indexes := make([]int, len(rr.Results))
	for i := range indexes {
		indexes[i] = i
	}
	sort.Slice(indexes, func(i, j int) bool {
		return rr.Results[indexes[i]].ProvisionalScore > rr.Results[indexes[j]].ProvisionalScore
	})
	place := 1
	for p, i := range indexes {
		if p > 0 && rr.Results[indexes[p-1]].ProvisionalScore > rr.Results[i].ProvisionalScore {
			place = p + 1
		}
		standings[i] = place
	}
	return standings
}
