package tcmmdatafeed

// author: Leonardone

// TopCoder MM DataFeeds
// community.topcoder.com/longcontest/?module=Static&d1=support&d2=dataFeed

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/neetsdkasu/tcutils/data"
	"path/filepath"
	"sort"
)

const (
	RatingHistoryPath       = "tc?module=BasicData&c=dd_marathon_rating_history&cr=%d"
	RatingHistoryDir        = "marathon_rating_history"
	RatingHistoryExpireDays = 7
	recordSeekDepth         = 5
)

func MakeRatingHistoryFileName(cr int64) string {
	return filepath.Join(RatingHistoryDir, fmt.Sprint(cr))
}

func MakeRatingHistoryPath(cr int64) string {
	return fmt.Sprintf(RatingHistoryPath, cr)
}

type Record struct {
	Handle        string  `xml:"handle"`
	CoderId       int64   `xml:"coder_id"`
	RoundId       int64   `xml:"round_id"`
	NumRatings    int     `xml:"num_ratings"`
	ProblemId     int64   `xml:"problem_id"`
	ShortName     string  `xml:"short_name"`
	Date          string  `xml:"date"`
	OldRating     int     `xml:"old_rating"`
	NewRating     int     `xml:"new_rating"`
	OldVolatility int     `xml:"old_volatility"`
	NewVolatility int     `xml:"new_volatility"`
	Rank          int     `xml:"rank"`
	Percentile    float64 `xml:"percentile"`
}

type RatingHistory struct {
	XMLName xml.Name  `xml:"dd_marathon_rating_history"`
	Records []*Record `xml:"row"`
}

func GetRatingHistory(cr int64, updating data.Updating) (*RatingHistory, error) {
	path := MakeRatingHistoryPath(cr)
	fileName := MakeRatingHistoryFileName(cr)
	d := NewData(path, fileName, RatingHistoryExpireDays, updating)
	v := new(RatingHistory)
	if err := d.Scan(v); err != nil {
		return nil, err
	}
	v.SortByRatingChange()
	return v, nil
}

func (rh *RatingHistory) SortByDate() {
	sort.Slice(rh.Records, func(i, j int) bool {
		return rh.Records[i].Date < rh.Records[j].Date
	})
}

func (rh *RatingHistory) moveRecord(from, to int) {
	tmp := rh.Records[from]
	if from < to {
		for k := from; k < to; k++ {
			rh.Records[k] = rh.Records[k+1]
		}
	} else {
		for k := from; k > to; k-- {
			rh.Records[k] = rh.Records[k-1]
		}
	}
	rh.Records[to] = tmp
}

func (rh *RatingHistory) SortByRatingChange() {
	rh.SortByDate()

	// move start rating
	for i := 0; i < len(rh.Records); i++ {
		if rh.Records[i].OldRating == 0 {
			rh.moveRecord(i, 0)
			break
		}
	}

	for i := 0; i < len(rh.Records); i++ {
		for j := i + 1; j < i+recordSeekDepth+1 && j < len(rh.Records); j++ {
			if rh.Records[i].NewRating == rh.Records[j].OldRating {
				rh.moveRecord(j, i+1)
				break
			}
		}
	}
}

func (rh *RatingHistory) GetNumRatingsOnRound(rd int64) int {
	for i, rec := range rh.Records {
		if rec.RoundId == rd {
			return i + 1
		}
	}
	return 0
}
